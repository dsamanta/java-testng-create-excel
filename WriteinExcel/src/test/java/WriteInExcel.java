import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;

import org.testng.annotations.Test;



public class WriteInExcel {
		

	    // File Name 
		String filename = System.getProperty("user.dir")  + "/src/main/resources/" + "NewExcelFile.xlsx" ;
		
		//Create blank workbook
	    XSSFWorkbook workbook = new XSSFWorkbook();
	    
	    //Create a blank sheet
	    XSSFSheet spreadsheet = workbook.createSheet( "APIDetails");


	    @Test(priority=1)
	    public void CreateExcel() throws IOException 
	    {
	    	System.out.println(System.getProperty("user.dir"));
	    	
	    	
	    	XSSFRow rowhead = spreadsheet.createRow((short)0);
	    	rowhead.createCell(0).setCellValue("No.");
	    	rowhead.createCell(1).setCellValue("Name");
	    	rowhead.createCell(2).setCellValue("Address");
	    	rowhead.createCell(3).setCellValue("Email");
	    	
	    	XSSFRow row = spreadsheet.createRow((short)1);
	        row.createCell(0).setCellValue("1");
	        row.createCell(1).setCellValue("Sankumarsingh");
	        row.createCell(2).setCellValue("India");
	        row.createCell(3).setCellValue("sankumarsingh@gmail.com");
	    }
	    
	    @Test(priority=2)
	    public void ExcelClose() throws IOException {
	    	
	        FileOutputStream fileOut = new FileOutputStream(filename);
	        workbook.write(fileOut);
	        fileOut.close();
	        workbook.close();
	        System.out.println("Your excel file has been generated!");
	    	
	    }
		
		
	}

